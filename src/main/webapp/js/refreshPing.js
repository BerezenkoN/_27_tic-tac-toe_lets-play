setInterval(function () {
  var body = $('body');
  var userGame = body.data('game');
  var isMyTurn = body.data('my-turn');
  var gameOver = body.data('game-over');

  if (isMyTurn !== true && gameOver !== true) {
    $.get('/game/need-refresh?user=' + userGame, function (data) {
      if (data === 'yes') window.location = window.location;
    });
  }
}, 1000);

<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <link rel="stylesheet" href="<c:url value="/css/style.css"/>">
</head>

<body data-game="${game.host}"
      data-my-turn="${sessionScope.username eq game.currentPlayer}"
      data-game-over="${game.gameOver}">
<c:if test="${sessionScope.username eq game.currentPlayer and not game.gameOver}">
    <h4>Your turn!</h4>
</c:if>

<c:if test="${game.gameOver}">
    <h4>Game over. Winner is ${game.winner}!</h4>
    <a href="<c:url value="/index?exitGame=true"/>">Back to Start</a>
</c:if>
<c:forEach var="i" begin="0" end="2">
    <div class="field-row">
    <c:forEach var="j" begin="0" end="2">
        <c:choose>
            <c:when test="${game.field[i][j] == 0}">
                <c:if test="${not game.gameOver}">
                    <a class="field-cell" href="<c:url value="/move?user=${param.user}&i=${i}&j=${j}"/>"></a>
                </c:if>
                <c:if test="${game.gameOver}">
                    <span class="field-cell"></span>
                </c:if>
            </c:when>
            <c:when test="${game.field[i][j] == 1}">
                <span class="field-cell">X</span>
            </c:when>
            <c:when test="${game.field[i][j] == 2}">
                <span class="field-cell">O</span>
            </c:when>
        </c:choose>
    </c:forEach>
    </div>
</c:forEach>
<c:if test="${not game.gameOver}">
    <script src="<c:url value="/js/jquery-3.1.1.min.js"/>"></script>
    <script src="<c:url value="/js/refreshPing.js"/>"></script>
</c:if>
</body>

</html>

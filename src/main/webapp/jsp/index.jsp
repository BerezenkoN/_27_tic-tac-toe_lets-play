<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
    <head>
        <meta http-equiv="refresh" content="5">
    </head>

    <body>
        <h4>Hello, ${sessionScope.username}</h4>

        <form action="<c:url value="/game"/>" method="post">
            <button>Create game</button>
        </form>

        <table>
            <c:forEach var="game" items="${games}">
                <tr>
                    <td>Play with <a href="<c:url value="/game?user=${game.host}"/>">${game.host}</a></td>
                </tr>
            </c:forEach>
        </table>
    </body>

</html>

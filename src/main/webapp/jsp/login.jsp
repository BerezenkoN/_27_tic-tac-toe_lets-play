<%--
  Created by IntelliJ IDEA.
  User: aam
  Date: 1/2/17
  Time: 2:00 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Login</title>
</head>
<body>
    <form action="/login" method="post">
        <label for="username">Enter user name:</label>
        <input id="username" type="text" name="username"/>
        <button type="submit">Enter</button>
    </form>
    <div id="body">
        <h1>${sessionScope.status}</h1>
        <div id="info">
            Hello! After Login, You can create game and invite a friend or join an already created game.<br />
            bitBucket repository: <a href="https://bitbucket.org/BerezenkoN/_27_tic-tac-toe_lets-play">https://bitbucket.org/BerezenkoN/_27_tic-tac-toe_lets-play</a>
        </div>
    </div>
</body>

</body>
</html>

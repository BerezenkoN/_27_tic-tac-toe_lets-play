package Krestiki;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class GameServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userGame = req.getParameter("user");
        String user = (String) req.getSession(false).getAttribute("username");

        if (req.getRequestURI().endsWith("/need-refresh")) {
            needRefresh(user, resp);
            return;
        }

        GamesContainer.Game game = GamesContainer.getGame(userGame);

        if (user.equals(game.getPlayer1()) || user.equals(game.getPlayer2())) {
            req.setAttribute("game", game);
            req.getRequestDispatcher("/jsp/game.jsp?user=" + userGame).forward(req, resp);
            return;
        }

        if (game.getPlayer2() != null) {
            resp.sendRedirect("/index");
            return;
        }

        if (!game.isGameOver()) {
            game.setPlayer2(user);
            req.setAttribute("game", game);
            req.getRequestDispatcher("/jsp/game.jsp?user=" + userGame).forward(req, resp);
        } else resp.sendRedirect("/index");
    }

    private void needRefresh(String user, HttpServletResponse response) throws IOException {
        response.setContentType("text/plain");
        GamesContainer.Game joinedGame = GamesContainer.getJoinedGame(user);
        if (user.equals(joinedGame.getCurrentPlayer()) || joinedGame.isGameOver()) {
            response.getWriter().write("yes");
        } else {
            response.getWriter().write("no");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String user = (String) req.getSession(false).getAttribute("username");
        GamesContainer.Game game = GamesContainer.getGame(user);
        if (game == null) {
            GamesContainer.createGame(user);
            resp.sendRedirect("/game?user=" + user);
        } else {
            resp.sendRedirect("/game?user=" + game.getHost());
        }
    }
}

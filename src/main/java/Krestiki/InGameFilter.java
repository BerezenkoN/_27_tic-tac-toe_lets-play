package Krestiki;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Optional;

public class InGameFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        Optional<HttpSession> session = Optional.ofNullable(req.getSession(false));
        Optional<String> username = session.map(s -> (String) s.getAttribute("username"));
        if (!username.isPresent()) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }
        GamesContainer.Game game = GamesContainer.getJoinedGame(username.get());
        if (game != null) {
            if (req.getRequestURI().equals("/game")) {
                filterChain.doFilter(servletRequest, servletResponse);
            } else {
                if (req.getRequestURI().equals("/index") && Boolean.valueOf(req.getParameter("exitGame"))) {
                    String user = (String) req.getSession(false).getAttribute("username");
                    GamesContainer.exitGame(user);
                    filterChain.doFilter(servletRequest, servletResponse);
                } else {
                    HttpServletResponse resp = (HttpServletResponse) servletResponse;
                    resp.sendRedirect("/game?user=" + game.getHost());
                }
            }
        } else {
            filterChain.doFilter(req, servletResponse);
        }
    }

    @Override
    public void destroy() {

    }
}

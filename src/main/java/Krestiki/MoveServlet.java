package Krestiki;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class MoveServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userGame = req.getParameter("user");
        String user = (String) req.getSession(false).getAttribute("username");
        int i = Integer.parseInt(req.getParameter("i"));
        int j = Integer.parseInt(req.getParameter("j"));

        GamesContainer.Game game = GamesContainer.move(userGame, user, i, j);
        req.setAttribute("game", game);
        resp.sendRedirect("/game?user=" + userGame);
    }
}

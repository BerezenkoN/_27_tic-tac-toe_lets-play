package Krestiki;

import java.util.*;
import java.util.stream.Collectors;

public class GamesContainer {

    private static Map<String, Game> games = new HashMap<>();
    private static int FIELD_SIZE = 3;
    private static Random rnd = new Random();

    public static class Game {

        private int[][] field = new int[FIELD_SIZE][FIELD_SIZE];
        int turn = 1;
        private String host;
        private String player1;
        private String player2;
        private String currentPlayer;
        private boolean gameOver;
        private String winner;
        private int moveCount;

        Game(String host) {
            this.host = host;
            this.player1 = host;
            for (int i = 0; i < FIELD_SIZE; i++) {
                for (int j = 0; j < FIELD_SIZE; j++) {
                    field[i][j] = 0;
                }
            }
        }

        public int[][] getField() {
            return field;
        }

        public void setPlayer2(String player2) {
            this.player2 = player2;
            currentPlayer = rnd.nextBoolean() ? player1 : player2;
        }

        public String getHost() {
            return host;
        }

        public String getPlayer1() {
            return player1;
        }

        public String getPlayer2() {
            return player2;
        }

        public void swapTurn() {
            turn = turn == 1 ? 2 : 1;
            currentPlayer = currentPlayer.equals(player1) ? player2 : player1;
        }

        public void move(int i, int j) {
            field[i][j] = turn;

            if (
                (field[0][0] != 0 && field[0][0] == field[0][1] && field[0][0] == field[0][2]) ||
                (field[1][0] != 0 && field[1][0] == field[1][1] && field[1][0] == field[1][2]) ||
                (field[2][0] != 0 && field[2][0] == field[2][1] && field[2][0] == field[2][2]) ||
                (field[0][0] != 0 && field[0][0] == field[1][0] && field[0][0] == field[2][0]) ||
                (field[0][1] != 0 && field[0][1] == field[1][1] && field[0][1] == field[2][1]) ||
                (field[0][2] != 0 && field[0][2] == field[1][2] && field[0][2] == field[2][2]) ||
                (field[0][0] != 0 && field[0][0] == field[1][1] && field[0][0] == field[2][2]) ||
                (field[0][2] != 0 && field[0][2] == field[1][1] && field[0][2] == field[2][0])
            ) {
                gameOver = true;
                winner = currentPlayer;
                return;
            } else {
                moveCount++;
                if (moveCount == 9) {
                    gameOver = true;
                    return;
                }
            }

            swapTurn();
        }

        public boolean isGameOver() {
            return gameOver;
        }

        public String getWinner() {
            return winner;
        }

        public String getCurrentPlayer() {
            return currentPlayer;
        }

        public void setPlayer1(String player1) {
            this.player1 = player1;
        }
    }

    public static Game move(String userGame, String user, int i, int j) {
        Game game = games.get(userGame);
        if (game.currentPlayer == null || !game.currentPlayer.equals(user) || game.field[i][j] != 0) return game;
        game.move(i, j);
        return game;
    }

    public static Game getGame(String user) {
        return games.get(user);
    }

    public static Game getJoinedGame(String username) {
        return games.values().stream()
                .filter(g -> username.equals(g.player1) || username.equals(g.player2))
                .findFirst()
                .orElse(null);
    }

    public static void exitGame(String user) {
        Game game = getJoinedGame(user);
        if (game != null) {
            if (user.equals(game.getPlayer1())) {
                game.setPlayer1(null);
            } else if (user.equals(game.getPlayer2())) {
                game.setPlayer2(null);
            }
            if (game.getPlayer1() == null && game.getPlayer2() == null) {
                games.remove(game.host);
            }
        }
    }

    public static Collection<Game> getHostedGames() {
        return games.values().stream().filter(g -> g.getPlayer2() == null).collect(Collectors.toList());
    }

    public static void createGame(String user) {
        games.put(user, new Game(user));
    }

}
